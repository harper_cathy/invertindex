import sys
from persist import load


def search(word):
    trie = load()
    records = trie.find(word)
    print("%s" % (word));
    for record in records:
        print("\tfile:%s, line %d, word %d" % (record['file'], record['line'], record['word']))

if __name__ == "__main__":
    search(sys.argv[1])
