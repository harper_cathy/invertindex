import sys
import re
from persist import load, save


def main(file):
    # Build the trie in memory
    trie = load()

    # Scan the file and get each word
    with open(file) as f:
        content = f.readlines()

        line_count = 0

        for line in content:
            # Count line number
            line_count += 1
            words = re.compile("[-\.,:;\s\n]+").split(line.lower())
            word_count = 0
            for word in words:
                word_count += 1
                trie.append(word, {"file": file, "line": line_count, "word": word_count})

    save(trie)

if __name__ == "__main__":
    main(sys.argv[1])
