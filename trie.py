class TrieNode:
    def __init__(self, c, leaf):
        self.char = c
        self.leaf = leaf
        self.children = {}
        self.payload = []

    def traverse(self):
        if self.leaf:
            yield self.char
        for ckey in self.children:
            for res in self.children[ckey].traverse():
                yield self.char + res


class Trie:
    root = TrieNode("", False)

    def __init__(self):
        pass

    def node(self, word, insert):
        current = self.root
        for i in range(len(word)):
            c = word[i]
            if c in current.children:
                current = current.children[c]
            else:
                if insert:
                    newnode = TrieNode(c, i == len(word) - 1)
                    current.children[c] = newnode
                    current = newnode
                else:
                    return None
        return current

    def insert(self, word, item):
        target = self.node(word, True)
        target.payload = item

    def append(self, word, item):
        target = self.node(word, True)
        target.payload.append(item)

    def find(self, word):
        target = self.node(word, False)
        if target is None:
            return []
        return target.payload

    def traverse(self):
        return self.root.traverse()
