from trie import Trie
import os
import json

storage = "storage.dat"


def load():
    trie = Trie()
    if os.path.isfile(storage):
        with open(storage, "r") as f:
            data = json.load(f)
        for key in data:
            trie.insert(key, data[key])
    return trie


def save(trie):
    data = {}
    for key in trie.traverse():
        value = trie.find(key)
        data[key] = value

    with open(storage, 'w') as outfile:
        json.dump(data, outfile)
